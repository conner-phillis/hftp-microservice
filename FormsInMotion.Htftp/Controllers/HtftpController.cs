﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace FormsInMotion.Htftp.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class HtftpController : ControllerBase
	{
		private const string OngoingUploadProcessFolder = "Uploads";

		private readonly IHostingEnvironment _hostingEnvironment;

		public HtftpController(IHostingEnvironment hostingEnvironment)
		{
			_hostingEnvironment = hostingEnvironment;
		}

		/// <summary>
		/// Create an upload process
		/// </summary>
		/// <param name="productId">the id of the product that this upload process will be used for</param>
		/// <returns>the status of the method</returns>
		public ActionResult<string> GenerateUploadJob(string productId)
		{
			//the guid acts as our unique identifier for the ongoing upload process
			Guid folderId = new Guid();
			try
			{
				//create a folder with the name of the guid
				DirectoryInfo directory = Directory.CreateDirectory($"{OngoingUploadProcessFolder}/{folderId}");
				//generate the productId file so we can keep track of it
				ActionResult<string> action = AddFileToJob(folderId, "process.db", Encoding.ASCII.GetBytes(productId));
				//if we can't generate it return the result of the bad upload & delete the directory
				if (action.Value != bool.TrueString)
				{
					directory.Delete();
					return action;
				}
			}
			catch (Exception ex)
			{
				return BadRequest(_hostingEnvironment.IsDevelopment() ? ex.ToString() : "Could not create upload process");
			}

			return Ok(folderId);
		}

		public IActionResult GenerateUploadJob(string productId, string configurationFile, byte[] fileBytes)
		{
			//generate the upload job
			ActionResult<string> uploadResult = GenerateUploadJob(productId);
			//if we can parse the GUID, then the GenerateUploadJob succeeded
			if (Guid.TryParse(uploadResult.Value, out Guid guid))
			{
				//append the file like a normal file
				ActionResult<string> addFileResult = AddFileToJob(guid, configurationFile, fileBytes);
				if (addFileResult.Value == bool.TrueString) //if success, return okay
					return Ok(guid);
				return BadRequest(addFileResult); //if failed, return the add file status
			}

			//if we can't parse the GUID, return the generate upload job result
			return BadRequest(uploadResult);
		}

		/// <summary>
		/// append a file to an ongoing job
		/// </summary>
		/// <param name="jobId">the job id the file will be appended to</param>
		/// <param name="fileName">the name of the file</param>
		/// <param name="fileBytes">the contents of the file</param>
		/// <returns>the status of the method</returns>
		public ActionResult<string> AddFileToJob(Guid jobId, string fileName, byte[] fileBytes)
		{
			try
			{
				//if the directory does not exist, then we should throw an exception and fail b/c there's
				//no ongoing process for the document
				using (StreamWriter fileStreamWriter = new StreamWriter(GetFilePath(jobId, fileName)))
				{
					fileStreamWriter.Write(fileBytes);
				}
			}
			catch (Exception ex)
			{
				return _hostingEnvironment.IsDevelopment() ? BadRequest(ex) : BadRequest($"Could not append file {fileName} to job {jobId}");
			}
			//we use TrueString so we can return arbitrary object types from here
			return Ok(true.ToString());
		}

		/// <summary>
		/// perform the work that is associated with a job
		/// </summary>
		/// <param name="guid"></param>
		/// <returns></returns>
		public ActionResult<string> PerformJob(Guid guid)
		{
			try
			{
				//get the directory for the job
				DirectoryInfo directory = new DirectoryInfo(GetDirectoryPath(guid));
				//enumerate the containing files
				foreach (FileInfo jobDocument in directory.GetFiles().Where(file => !file.Name.EndsWith(".db")))
					jobDocument.Delete();
				//delete the directory
				directory.Delete();
				//we use TrueString so we can use arbitrary request strings
				return Ok(true.ToString());
			}
			catch (Exception ex)
			{
				return BadRequest(_hostingEnvironment.IsDevelopment() ? ex.ToString() : $"could not close job {guid}");
			}
		}

		private string GetDirectoryPath(Guid jobId)
		{
			return $"{OngoingUploadProcessFolder}/{jobId}";
		}

		private string GetFilePath(Guid jobId, string fileName)
		{
			return $"{GetDirectoryPath(jobId)}/{fileName}";
		}
	}
}